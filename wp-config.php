<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5K`h%<Xi6Oxvj,PGn!+w} xeb7*Ux[s;<.EX>]IbFbzdL(.XN+D<ZF Wm-Elo4@t');
define('SECURE_AUTH_KEY',  'co`XZN^m;0hISYSz`*n:E2i2Mgs H>44zpPV}!.b9fB_:>.4eSVx];!JO)..%!*.');
define('LOGGED_IN_KEY',    'l@y:c7I$(AqTc2RDx/ILp+liR|.IfFBB0{!d4C`U4p5Ia%3a$(kC/32RT40+U QR');
define('NONCE_KEY',        'ovCUVPd`ahx]z88DY8A+/EaleDsu>J>;Y8LyxJ^m:mwez2IZ}So(wS(:Nl{@@wD0');
define('AUTH_SALT',        '_Ah_=?u8i-4hb`,8X.=*Sn//K$YjmIf]B 5 $~r:FO[[j^I/+Pmv-[>tKROlXR7K');
define('SECURE_AUTH_SALT', '?iFQVX#fO57tdSGmyj|O>Q.04yHQp)R7?WOH`PF{^af&o}tZ8^5T<^}1UPE3Sh;g');
define('LOGGED_IN_SALT',   'b}hyD1jI)B1$|kDqm<,(-^fEfb%DgQQ`^u/ruX%BNM8)Q{hfT6W5py3ywkV5(_(B');
define('NONCE_SALT',       '/|K^29rE#gt8K+c[i.uo(K%`2eE1=&05vCvrsa~F|y,%gD) j%/|DRY0i7:I]c12');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
